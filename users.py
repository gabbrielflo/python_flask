from login import auth_required
from flask import Flask, json, jsonify,request
from recordsList import data
import psycopg2
from functools import wraps

app = Flask(__name__)

#conexion = psycopg2.connect(host="192.168.0.171", database="endpoint3", user="posgres", password="password")

@app.route('/user')
@auth_required
def getUsers():
    return jsonify(data)

@app.route('/user/<string:user_name>')
@auth_required
def getUser(user_name):
    userfound = [user for user in data if user['name'] == user_name]
    if(len(userfound)>0):
        return jsonify(userfound)
    return jsonify({"messaje":"user not found"})

@app.route('/user', methods=['POST'])
@auth_required
def addUser():
    new_user={
        "name":request.json["name"],
        "tel":request.json["tel"],
        "email":request.json["email"]
    }
    data.append(new_user)
    return jsonify({"messaje":"user added","usuario":data})

@app.route('/user/<string:user_name>', methods=['PUT'])
@auth_required
def updateUser(user_name):
    userfound = [user for user in data if user['name'] == user_name]
    if(len(userfound)>0):
        userfound[0]["name"]= request.json["name"]
        userfound[0]["tel"]= request.json["tel"]
        userfound[0]["email"]= request.json["email"]
        return jsonify({
            "message": "user updted",
            "user":userfound[0]
        })
    return jsonify({"messaje":"user not found"})

@app.route('/user/<string:user_name>', methods=['DELETE'])
@auth_required
def deleteUser(user_name):
    userfound = [user for user in data if user['name'] == user_name]
    if(len(userfound)>0):
        data.remove(userfound[0])
        return jsonify({
            "message": "user deleted",
            "user":data
        })
    return jsonify({"messaje":"user not found"})

if __name__ == '__main__':
    app.run(debug=True, port=3000)