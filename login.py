from flask import Flask, json, jsonify,request
from functools import wraps

app = Flask(__name__)

app.config["SECRET_KEY"] ="secretkey"

def auth_required(f):
    @wraps(f)
    def decorator(*args, **kwargs):
        auth = request.authorization
        if auth and auth.username == 'admin' and auth.password =='password':
            return f(*args, **kwargs)

        return jsonify('Please verify you login!', 401,{'AuthFailureError':'Login Required'})

    return decorator

@app.route('/')
def login():
    if request.authorization and request.authorization.username =='admin' and request.authorization.password =='password':
        return ""
    
    return jsonify({'statusCode:':401,'message':'AuthFailureError=Login Required'})


@app.errorhandler(500)
def internal_error(error):
    return "500 ServerError",500

@app.errorhandler(404)
def not_found(error):
    return "404 page not found ",404

if __name__ == '__main__':
    app.run(debug=True, port=3000)