from login import auth_required
from flask import Flask, json, jsonify,request

app = Flask(__name__)

@app.route('/balance', methods = ['POST'])
@auth_required
def balance():
    input_data={
        'Mes': request.json["Mes"],
        'Ventas':request.json['Ventas'],
        'Gastos':request.json['Gastos']
        }
    list_balance=[]
    
    for mes,ventas,gastos in zip(input_data["Mes"],input_data["Ventas"], input_data["Gastos"]):
        diccionario={}
        op_balance=(ventas)-(gastos)
        diccionario["Mes"] =mes
        diccionario["Ventas"] = ventas
        diccionario["Gastos"] = gastos
        diccionario["Balace"] = op_balance
        list_balance.append(diccionario)
    return jsonify(list_balance)

if __name__ == '__main__':
    app.run(debug=True, port=3000)